<?php


class BITM{

    public $window;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;

//method1
    public function CoolTheAir(){


        echo "I am cooling the air";
    }
//method 2
    public function compute(){

        echo " i am computing";

    }
//method3

    public function show(){

        echo  $this->window."</br>";
        echo  $this->door."</br>";
        echo  $this->chair."</br>";
        echo  $this->table."</br>";
        echo  $this->whiteboard."</br>";


    }



    public function setChair($chair){

        $this->chair=$chair."[set by parent]";

    }

}//end of BITM CLASS




$myobject=new BITM();
$myobject->setChair("I am a chair");
$myobject->show();





class BITM_Lab402 extends BITM{


public function setChairFromChild($child_chair)
{

    $child_chair=$child_chair."[set by child]";
    parent::setChair($child_chair);

}


}


$myParentobj=new BITM();
$myParentobj->setChair("i am a chair");
$myParentobj->show();



$mychildobj=new BITM_Lab402();
$mychildobj->setChair("this is a chair");
$mychildobj->show();


echo $mychildobj->chair."</br>";
$mychildobj->setChairFromChild("Ami ekjon manush");
echo $mychildobj->chair."</br>";